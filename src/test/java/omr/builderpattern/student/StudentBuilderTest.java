package omr.builderpattern.student;

import omr.builderpattern.bankaccount.BankAccount;
import omr.builderpattern.bankaccount.InvalidBankAccountException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class StudentBuilderTest {

    @Test
    public void when_allValuesForStudentAreValid_expect_success() {
        Student student = new Student.Builder().withFirstName("Joe")
                                               .withLastName("Bloggs")
                                               .withDOB(LocalDate.of(1970, 1, 1))
                                               .withUniversity("UCL")
                                               .withStudentId(12345678)
                                               .build();
        Assertions.assertThat(student)
                  .isNotNull();
        Assertions.assertThat(student.getFirstName())
                  .isEqualTo("Joe");
        Assertions.assertThat(student.getLastName())
                  .isEqualTo("Bloggs");
        Assertions.assertThat(student.getDOB())
                  .isEqualTo(LocalDate.of(1970, 1, 1));
        Assertions.assertThat(student.getUniversity())
                  .isEqualTo("UCL");
        Assertions.assertThat(student.getStudentId())
                  .isEqualTo(12345678);
        Assertions.assertThat(student.toString())
                  .isNotEmpty()
                  .startsWith("Student{");
    }

    @Test
    public void when_justStudentIdPassed_expect_success() {
        Student student = new Student.Builder().withStudentId(12345678)
                                               .build();
        Assertions.assertThat(student)
                  .isNotNull();
        Assertions.assertThat(student.getFirstName())
                  .isNull();
        Assertions.assertThat(student.getLastName())
                  .isNull();
        Assertions.assertThat(student.getDOB())
                  .isNull();
        Assertions.assertThat(student.getUniversity())
                  .isNull();
        Assertions.assertThat(student.getStudentId())
                  .isEqualTo(12345678);
        Assertions.assertThat(student.toString())
                  .isNotEmpty()
                  .isEqualTo(
                          "Student{studentId=12345678, firstName='null', lastName='null', dob=null, university='null'}");
    }

    @Test
    public void when_nothingPassed_expect_exception() {
        InvalidStudentException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidStudentException.class,
                () -> new Student.Builder().build(),
                "Exception should have been thrown as student id was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("Invalid student: no student id");
    }

    @Test
    public void when_justFirstNamePassed_expect_exception() {
        InvalidStudentException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidStudentException.class,
                () -> new Student.Builder().withFirstName("Joe")
                                           .build(),
                "Exception should have been thrown as student id was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("Invalid student: no student id");
    }

    @Test
    public void when_justLastNamePassed_expect_exception() {
        InvalidStudentException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidStudentException.class,
                () -> new Student.Builder().withLastName("Bloggs")
                                           .build(),
                "Exception should have been thrown as student id was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("Invalid student: no student id");
    }
}
