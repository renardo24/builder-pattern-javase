package omr.builderpattern.bankaccount;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class BankAccountBuilderTest {

    @Test
    public void when_allValuesForAccountAreValid_expect_success() {
        BankAccount bankAccount = new BankAccount.Builder().withAccountNumber(12345678)
                                                           .withSortCode(123456)
                                                           .withOwnerFullName("Joe Bloggs")
                                                           .withBalance(30.0)
                                                           .withBranch("Local branch")
                                                           .withInterestRate(0.05)
                                                           .build();
        Assertions.assertThat(bankAccount)
                  .isNotNull();
        Assertions.assertThat(bankAccount.getAccountNumber())
                  .isEqualTo(12345678);
        Assertions.assertThat(bankAccount.getSortCode())
                  .isEqualTo(123456);
        Assertions.assertThat(bankAccount.getOwnerFullName())
                  .isEqualTo("Joe Bloggs");
        Assertions.assertThat(bankAccount.getBalance())
                  .isEqualTo(30.0);
        Assertions.assertThat(bankAccount.getInterestRate())
                  .isEqualTo(0.05);
        Assertions.assertThat(bankAccount.getBranch())
                  .isEqualTo("Local branch");
        Assertions.assertThat(bankAccount.toString())
                  .isNotEmpty()
                  .startsWith("BankAccount{");
    }

    @Test
    public void when_nothingPassed_expect_exception() {
        InvalidBankAccountException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidBankAccountException.class,
                () -> new BankAccount.Builder().build(),
                "Exception should have been thrown as account number was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("AccountNumber or SortCode invalid");
    }

    @Test
    public void when_justAccountNumberPassed_expect_exception() {
        InvalidBankAccountException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidBankAccountException.class,
                () -> new BankAccount.Builder().withAccountNumber(123456678)
                                               .build(),
                "Exception should have been thrown as sortcode was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("AccountNumber or SortCode invalid");
    }

    @Test
    public void when_justSortCodePassed_expect_exception() {
        InvalidBankAccountException ex = org.junit.jupiter.api.Assertions.assertThrows(
                InvalidBankAccountException.class,
                () -> new BankAccount.Builder().withSortCode(123456)
                                               .build(),
                "Exception should have been thrown as account number was not specified.");
        Assertions.assertThat(ex.getMessage())
                  .isEqualTo("AccountNumber or SortCode invalid");
    }
}
