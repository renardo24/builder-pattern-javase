package omr.builderpattern.bankaccount;

public class InvalidBankAccountException extends RuntimeException {
    public InvalidBankAccountException(final String message) {
        super(message);
    }
}
