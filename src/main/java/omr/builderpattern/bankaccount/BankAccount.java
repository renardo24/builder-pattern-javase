package omr.builderpattern.bankaccount;

public class BankAccount {

    private int accountNumber;
    private int sortCode;
    private String ownerFullName;
    private String branch;
    private double interestRate;
    private double balance;

    /**
     * private constructor with the required fields
     */
    private BankAccount(final int accountNumber, final int sortCode) {
        this.accountNumber = accountNumber;
        this.sortCode = sortCode;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getSortCode() {
        return sortCode;
    }

    public String getOwnerFullName() {
        return ownerFullName;
    }

    public String getBranch() {
        return branch;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountNumber=" + accountNumber +
                ", sortCode=" + sortCode +
                ", ownerFullName='" + ownerFullName + '\'' +
                ", branch='" + branch + '\'' +
                ", interestRate=" + interestRate +
                ", balance=" + balance +
                '}';
    }

    public static class Builder {
        private int accountNumber;
        private int sortCode;
        private String ownerFullName;
        private String branch;
        private double interestRate;
        private double balance;

        public Builder withAccountNumber(final int accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder withSortCode(final int sortCode) {
            this.sortCode = sortCode;
            return this;
        }

        public Builder withOwnerFullName(final String ownerFullName) {
            this.ownerFullName = ownerFullName;
            return this;
        }

        public Builder withBranch(final String branch) {
            this.branch = branch;
            return this;
        }

        public Builder withInterestRate(final double interestRate) {
            this.interestRate = interestRate;
            return this;
        }

        public Builder withBalance(final double balance) {
            this.balance = balance;
            return this;
        }

        public BankAccount build() {
            if (accountNumber <= 0 || sortCode <= 0) {
                throw new InvalidBankAccountException("AccountNumber or SortCode invalid");
            }
            BankAccount account = new BankAccount(accountNumber, sortCode);
            account.balance = this.balance;
            account.branch = this.branch;
            account.interestRate = this.interestRate;
            account.ownerFullName = this.ownerFullName;
            return account;
        }
    }
}
