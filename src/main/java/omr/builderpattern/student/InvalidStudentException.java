package omr.builderpattern.student;

public class InvalidStudentException extends RuntimeException {
    public InvalidStudentException(final String message) {
        super(message);
    }
}
