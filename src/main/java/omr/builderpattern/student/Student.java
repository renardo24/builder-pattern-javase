package omr.builderpattern.student;

import java.time.LocalDate;
import java.util.Objects;

public class Student {

    private Integer studentId;
    private String firstName;
    private String lastName;
    private LocalDate dob;
    private String university;

    private Student(final Integer studentId) {
        this.studentId = studentId;
    }


    public Integer getStudentId() {
        return studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public LocalDate getDOB() {
        return dob;
    }

    public String getUniversity() {
        return university;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                ", university='" + university + '\'' +
                '}';
    }

    public static class Builder {

        private Integer studentId;
        private String firstName;
        private String lastName;
        private LocalDate dob;
        private String university;

        public Builder withStudentId(final Integer studentId) {
            this.studentId = studentId;
            return this;
        }

        public Builder withFirstName(final String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName(final String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withUniversity(final String university) {
            this.university = university;
            return this;
        }

        public Builder withDOB(final LocalDate dob) {
            this.dob = dob;
            return this;
        }

        public Student build() {
            if (Objects.isNull(this.studentId)) {
                throw new InvalidStudentException("Invalid student: no student id");
            }
            Student student = new Student(this.studentId);
            student.dob = this.dob;
            student.firstName = this.firstName;
            student.lastName = this.lastName;
            student.university = this.university;

            return student;
        }

    }
}
