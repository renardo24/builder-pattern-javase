# Builder pattern using Java SE

Examples of using the builder pattern.

Some important points worth noting:

* Do the validation in the `build` method as this avoids any potential
concurrency issues.
* Include a constructor for the main bean that includes the minimum data
required for the object to be valid.
